package io;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.swing.JFileChooser;

import components.Edge;
import components.Node;
import io.DataHandler;

public class GraphGenerator {

	HashMap<String, Node> nodes;
	Set<Edge> edges;
	DataHandler dataHandler = new DataHandler();
	
	public GraphGenerator() {}
	
    /**
     * Generates a directed graph and stores it in a text document. 
     * 
     * @param nodesNr   number of nodes
     * @param edgesNr   number of edges
     */
	public void generateDirectedGraph ( int nodesNr, int edgesNr ) {
		nodes = new HashMap<String, Node>();
		edges = new HashSet<Edge>();
		Node nodex, node1, node2;
		Edge edgex;
		for (int i=1; i <= nodesNr; i ++) {
			nodex = new Node(""+i);
			nodes.put(""+i, nodex);			
		}
		
		Random random = new Random();
		int v_int2, v_int1;
		for (int i=1; i <= edgesNr; i ++) {	
			//generating random edges as long as they don't already exist
			do{ 
			v_int1 = random.nextInt(nodesNr)+1; 
			v_int2 = random.nextInt(nodesNr)+1;						
			int weight = random.nextInt(19)+1;
			node1=nodes.get(""+v_int1);
			node2=nodes.get(""+v_int2);
			edgex = new Edge(node1, node2 , true, null, weight);
			} while (edges.contains(edgex)); 
			edges.add(edgex);
			node1.addNeighbor(edgex);
			node2.addNeighbor(edgex);			
		}
		
		dataHandler.setEdges(edges);
		dataHandler.setNodes(nodes);
		
		// saving the generated graph in a file
		final JFileChooser fc = new JFileChooser();
		int returnVal = fc.showSaveDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fc.getSelectedFile();
			DataHandler.save(selectedFile, dataHandler);
		}
	}
	
}
