/**
 * Class used for loading and saving to/from file. And storage of Nodes/Edges of the current Graph.
 * 
 * @author Alexander Reichart, Andrada Boldis
 * @date October 2016
 */

package io;

import algorithms.*;
import components.Edge;
import components.Node;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataHandler {

	// Search patterns for dissecting graph inputs.
	Pattern firstNodePattern = Pattern.compile("(^[a-zA-Z0-9�������]+)");
	Pattern edgePattern = Pattern.compile("(->|--)");
	Pattern secondNodePattern = Pattern.compile("(->|--)\\s*([a-zA-Z0-9�������]+)");
	Pattern edgeNamePattern = Pattern.compile("(\\(\\s*)([a-zA-Z0-9�������-]+)(\\s*\\))");
	Pattern edgeWeightPattern = Pattern.compile("(:)\\s*(-?[0-9]+)");

	HashMap<String, Node> nodes = null;
	Set<Edge> edges = null;

	Boolean directed = null;

	public DataHandler() {
		init();
	}

	private void init() {
		this.nodes = new HashMap<String, Node>();
		this.edges = new HashSet<Edge>();
		this.directed = null;
	}

	/**
	 * Loads the selected file and combs through it line by line
	 * 
	 * @param String
	 *            Path of the file to load
	 * @return Boolean True if loaded successfully
	 */
	
	public Boolean load(String fileName) {
		init();
		FileReader fr;
		try {
			fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				parseInput(line);
			}

			if (this.directed == null)
				this.directed = false;

			br.close();
			fr.close();
			return true;
		} catch (FileNotFoundException e) {
			System.out.println("The system cannot find the file specified");
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * Creates the nodes and edges contained in line and stores them away
	 * 
	 * @param String
	 *            The line to be taken apart
	 */
	private void parseInput(String line) {
		Node firstNode = null;
		Node secondNode = null;
		Edge edge = null;
		Boolean direction = null;
		String edgeName = null;
		Integer edgeWeight = null;

		// Retrieving of data segments using regex matches (and creating nodes
		// unless they already exist)
		Matcher matcher = firstNodePattern.matcher(line);
		if (matcher.find()) {
			firstNode = this.nodes.containsKey(matcher.group(1)) ? this.nodes.get(matcher.group(1))
					: new Node(matcher.group(1));
		}
		matcher = secondNodePattern.matcher(line);
		if (matcher.find()) {
			secondNode = this.nodes.containsKey(matcher.group(2)) ? this.nodes.get(matcher.group(2))
					: new Node(matcher.group(2));
		}
		matcher = edgePattern.matcher(line);
		if (matcher.find()) {
			direction = matcher.group(1).equals("--") ? false : true;
		}
		matcher = edgeNamePattern.matcher(line);
		if (matcher.find()) {
			edgeName = matcher.group(2);
		}
		matcher = edgeWeightPattern.matcher(line);
		if (matcher.find()) {
			edgeWeight = Integer.parseInt(matcher.group(2));
		}

		// Storing of existing nodes
		if (firstNode != null)
			this.nodes.put(firstNode.getLabel(), firstNode);

		if (secondNode != null)
			this.nodes.put(secondNode.getLabel(), secondNode);

		// Creation of the edge if an edge exists, passing all parsed data or
		// alternatively null.
		// As well as storing the edge and adding it to both nodes.
		if (direction != null) {
			edge = new Edge(firstNode, secondNode, direction, edgeName, edgeWeight);

			this.edges.add(edge);
			firstNode.addNeighbor(edge);
			secondNode.addNeighbor(edge);

			if (this.directed == null)
				this.directed = edge.getDirected();
		}
	}

	/**
	 * 
	 * @return Boolean True if current Graph data is directed, decided by first
	 *         Edge
	 */
	public Boolean getDirected() {
		return directed;
	}

	/**
	 * 
	 * @return HashMap<String, Node> A HashMap with the nodes of the current
	 *         Data
	 */
	public HashMap<String, Node> getNodes() {
		return this.nodes;
	}

	/**
	 * 
	 * @return Set<Edge> A Set of the edges of the current Data
	 */
	public Set<Edge> getEdges() {
		return this.edges;
	}

	public void setEdges(Set<Edge> edges) {
		this.edges = edges;
	}
	
	public void setNodes(HashMap<String, Node> nodes) {
		this.nodes = nodes;
	}
	
	/**
	 * 
	 * @param File
	 *            Where to save
	 * @param DataHandler
	 *            the current datahandler
	 */
	public static void save(File selectedFile, DataHandler dataHandler) {
		try {
			// Create the file if it doesn't exist
			if (!selectedFile.exists()) {
				selectedFile.createNewFile();
			}

			// Initialization of the writer and creation of local copy of the
			// data
			BufferedWriter out = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(selectedFile.getAbsoluteFile()), "UTF-8"));
			HashMap<String, Node> nodes = new HashMap<String, Node>();
			nodes.putAll(dataHandler.getNodes());
			Set<Edge> edges = new HashSet<Edge>();
			edges.addAll(dataHandler.getEdges());

			String outputText = "";

			for (Edge edge : edges) {
				if (outputText.length() > 0)
					out.newLine();

				outputText = edge.getOne().getLabel() + " " + (edge.getDirected() ? "-> " : "-- ")
						+ edge.getTwo().getLabel() + (edge.getWasnamed() ? (" (" + edge.getName() + ")") : "")
						+ (edge.getWeight() == null ? "" : " : " + edge.getWeight()) + ";";
				// Removes nodes from the Map so in the end only nodes without
				// edges remain in it
				nodes.remove(edge.getOne().getLabel());
				nodes.remove(edge.getTwo().getLabel());
				out.write(outputText);

			}

			// Writing the remaining single nodes
			for (Node node : nodes.values()) {
				if (outputText.length() > 0)
					out.newLine();

				outputText = node.getLabel() + ";";
				out.write(outputText);
			}

			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
