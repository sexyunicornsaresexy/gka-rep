/**
 * This class models a Node in a graph. 
 * @author Alexander Reichart, Andrada Boldis
 * @date October 2016
 */

package components;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Node {

    private ArrayList<Edge> neighborhood;
    private Map<Node,Edge> connectingEdges;
    private String label;
    
    /**
     * 
     * @param String label The unique label associated with this Node
     */
    public Node(String label){
        this.label = label;
        this.neighborhood = new ArrayList<Edge>();
        this.connectingEdges = new HashMap<Node,Edge>();
    }
    
    
    /**
     * Adds an edge if it doesn't already exist 
     * 
     * @param Edge edge The edge to add
     */
    public void addNeighbor(Edge edge){
        if(this.neighborhood.contains(edge)){
            return;
        }
        this.neighborhood.add(edge);
       	connectingEdges.put(edge.getNeighbor(this), edge);
    }
    
    public Edge getConnection(Node node){
    	return connectingEdges.get(node);
    }
    
    /**
     * 
     * @param Edge other The edge for which to search
     * @return Boolean True if other is contained in this.neighborhood
     */
    public boolean containsNeighbor(Edge other){
        return this.neighborhood.contains(other);
    }
    
    /**
     * 
     * @param index The index of the Edge to retrieve
     * @return Edge The Edge at the specified index in this.neighborhood
     */
    public Edge getNeighbor(int index){
        return this.neighborhood.get(index);
    }
    
    
    /**
     * 
     * @param index The index of the edge to remove from this.neighborhood
     * @return Edge The removed Edge
     */
    public Edge removeNeighbor(int index){
        return this.neighborhood.remove(index);
    }
    
    /**
     * 
     * @param e The Edge to remove from this.neighborhood
     */
    public void removeNeighbor(Edge e){
        this.neighborhood.remove(e);
    }
    
    
    /**
     * 
     * @return int The number of neighbors of this Node
     */
    public int getNeighborCount(){
        return this.neighborhood.size();
    }
    
    
    /**
     * 
     * @return String The label of this Node
     */
    public String getLabel(){
        return this.label;
    }
    
    
    /**
     * 
     * @return String A String representation of this Node
     */
    public String toString(){
        return "Node " + label;
    }
    
    /**
     * 
     * @return int The hash code of this Node's label
     */
	public int hashCode() {
		return this.label.hashCode();
	}

	/**
	 * 
	 * @param Object
	 *            other The object to compare
	 * @return Boolean true if other instanceof Node and the two Node objects
	 *         have the same label
	 */
	public boolean equals(Object other) {
		if (!(other instanceof Node)) {
			return false;
		}

		Node v = (Node) other;
		return this.label.equals(v.label);
	}

	/**
	 * 
	 * @return ArrayList<Edge> A copy of this.neighborhood.
	 */
	public ArrayList<Edge> getNeighbors() {
        return new ArrayList<Edge>(this.neighborhood);
    }
    
}
