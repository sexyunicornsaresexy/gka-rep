package components;

/**
 * This class is used for the edges of the graphs. Both directed and undirected.
 * 
 * @author Alexander Reichart, Andrada Boldis
 * @date October 2016
 */
public class Edge implements Comparable<Edge> {

	private Node one, two;
	private Integer weight = null;
	private String name = null;
	private Boolean wasnamed = null;

	private Boolean directed = null;

	/**
	 * Two nodes and directed/undirected declaration required, fill in null for
	 * name and weight
	 * 
	 * @param one
	 *            The first Node in the Edge
	 * @param two
	 *            The second Node of the Edge
	 * @param directed
	 *            Edge directed or undirected
	 * @param name
	 *            The name of this Edge
	 * @param weight
	 *            The weight of this Edge
	 */
	public Edge(Node one, Node two, Boolean directed, String name, Integer weight) {
		this.one = one;
		this.two = two;
		this.directed = directed;

		this.weight = weight == null ? null : weight;

		if (name == null) {
			this.name = one.getLabel() + "-" + two.getLabel();
			this.wasnamed = false;
		} else {
			this.name = name;
			this.wasnamed = true;
		}

	}

	/**
	 * 
	 * @return Node The first node
	 */
	public Node getOne() {
		return this.one;
	}

	/**
	 * 
	 * @return Node The second node
	 */
	public Node getTwo() {
		return this.two;
	}

	/**
	 * 
	 * @return Boolean True if Edge is directed
	 */
	public Boolean getDirected() {
		return directed;
	}

	/**
	 *
	 * @return Integer The weight of this Edge
	 */
	public Integer getWeight() {
		return this.weight;
	}

	/**
	 * 
	 * @param weight
	 *            The new weight of this Edge
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 *
	 * @return String The name of this Edge
	 */
	public String getName() {
		return this.name;
	}

	/**
	 *
	 * @return Boolean true if node came with a name
	 */
	public Boolean getWasnamed() {
		return wasnamed;
	}

	/**
	 * 
	 * @param Node
	 *            current
	 * @return Node The neighbor of current along this Edge
	 */
	public Node getNeighbor(Node current) {
		if (!(current.equals(one) || current.equals(two))) {
			return null;
		}

		return (current.equals(one)) ? two : one;
	}

	/**
	 * Compares weight of other Edge with this Edge
	 * 
	 * @param other
	 *            The Edge to compare against this
	 * @return int this.weight - other.weight
	 */
	public int compareTo(Edge other) {
		return this.weight - other.weight;
	}

	/**
	 * 
	 * @return String A String representation of this Edge
	 */
	public String toString() {
		String returnText = getOne().getLabel() + " " + (getDirected() ? "-> " : "-- ") + getTwo().getLabel()
				+ (getWasnamed() ? " (" + getName() + ")" : "") + (getWeight() == null ? "" : " : " + getWeight())
				+ ";";
		return returnText;
	}

	/**
	 * 
	 * @return int The hash code for this Edge
	 */
	public int hashCode() {
		return (one.getLabel() + two.getLabel()).hashCode();
	}

	/**
	 * 
	 * @param other
	 *            The Object to compare against this
	 * @return true if other is an Edge with the same Nodes as this
	 */
	public boolean equals(Object other) {
		if (!(other instanceof Edge)) {
			return false;
		}

		Edge e = (Edge) other;

		return e.one.equals(this.one) && e.two.equals(this.two);
	}
}
