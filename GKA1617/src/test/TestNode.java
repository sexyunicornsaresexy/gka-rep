package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import components.Edge;
import components.Node;

public class TestNode {

	/*
	 * Test of the functions for adding and removing of Neighbors and
	 * getneighborcount
	 */
	@Test
	public void testAddRemoveNeighbors() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Node node3 = new Node("node3");

		Edge edge1 = new Edge(node1, node2, false, null, null);
		Edge edge2 = new Edge(node2, node3, false, null, null);

		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);
		assertTrue(node2.getNeighborCount() == 1);
		node2.addNeighbor(edge2);
		node3.addNeighbor(edge2);
		assertTrue(node2.getNeighborCount() == 2);

		assertTrue(node1.getNeighborCount() == 1);
		node1.removeNeighbor(edge1);
		assertTrue(node1.getNeighborCount() == 0);

		assertTrue(node2.getNeighbor(0) == edge1);
		assertTrue(node2.getNeighbor(1) == edge2);

		assertTrue(node2.containsNeighbor(edge1));
		assertTrue(node2.removeNeighbor(0) == edge1);
		assertTrue(node2.getNeighbor(0) == edge2);
		assertFalse(node2.containsNeighbor(edge1));

	}

	/*
	 * Test of getLabel() function
	 */
	@Test
	public void testGetLabel() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Node node3 = new Node("node3");
		assertEquals(node1.getLabel(), "node1");
		assertEquals(node2.getLabel(), "node2");
		assertEquals(node3.getLabel(), "node3");

	}

	/*
	 * Test of equals() function
	 */
	@Test
	public void testEquals() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node1");
		Node node3 = new Node("node3");

		assertTrue(node1.equals(node2));
		assertFalse(node1.equals(node3));

	}

	/*
	 * Test of getNeighbors()
	 */
	@Test
	public void testGetNeighbors() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Node node3 = new Node("node3");

		Edge edge1 = new Edge(node1, node2, false, null, null);
		Edge edge2 = new Edge(node2, node3, false, null, null);

		node2.addNeighbor(edge1);
		node2.addNeighbor(edge2);

		ArrayList<Edge> created = new ArrayList<Edge>();

		created.add(edge1);
		created.add(edge2);

		assertEquals(node2.getNeighbors(), created);
	}

}