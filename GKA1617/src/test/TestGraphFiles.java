package test;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Set;

import org.junit.Test;

import algorithms.BreadthFirst;
import components.Edge;
import components.Node;
import io.DataHandler;

public class TestGraphFiles {

	/*
	 * Test of graph01.gka
	 */
	@Test
	public void testgraph01() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\graph01.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		BreadthFirst bfs = new BreadthFirst(nodes, edges);

		assertEquals(bfs.traverse("j", "k"), "j > k Steps taken: 1");
		assertEquals(bfs.traverse("a", "b"), "a > b Steps taken: 1");
		assertEquals(bfs.traverse("a", "h"), "a > c > l > h Steps taken: 3");
		assertEquals(bfs.traverse("c", "g"), "c > d > k > g Steps taken: 3");
	}

	/*
	 * Test of graph02.gka
	 */
	@Test
	public void testgraph02() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\graph02.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		BreadthFirst bfs = new BreadthFirst(nodes, edges);

		assertEquals(bfs.traverse("g", "j"), "g > b > j Steps taken: 2");
		assertEquals(bfs.traverse("a", "b"), "a > b Steps taken: 1");
		assertEquals(bfs.traverse("a", "h"), "a > f > h Steps taken: 2");
		assertEquals(bfs.traverse("c", "g"), "c > k > g Steps taken: 2");
	}

	/*
	 * Test of graph03.gka
	 */
	@Test
	public void testgraph03() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\graph03.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		BreadthFirst bfs = new BreadthFirst(nodes, edges);

		assertEquals(bfs.traverse("Paderborn", "Hamburg"), "Paderborn > Hamburg Steps taken: 1");
		assertEquals(bfs.traverse("Hamburg", "Cuxhaven"), "Hamburg > Bremen > Cuxhaven Steps taken: 2");
		assertEquals(bfs.traverse("Minden", "Oldenburg"), "Minden > Hannover > Oldenburg Steps taken: 2");
		assertEquals(bfs.traverse("Hameln", "Hamburg"), "Hameln > Walsrode > Hamburg Steps taken: 2");
	}

	/*
	 * Test of graph04.gka
	 */
	@Test
	public void testgraph04() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\graph04.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		BreadthFirst bfs = new BreadthFirst(nodes, edges);

		assertEquals(bfs.traverse("v3", "v2"), "v3 > s > v2 Steps taken: 2");
		assertEquals(bfs.traverse("v1", "v5"), "v1 > q > v5 Steps taken: 2");
		assertEquals(bfs.traverse("v2", "q"), "v2 > v4 > q Steps taken: 2");
		assertEquals(bfs.traverse("v7", "v2"), "v7 > s > v2 Steps taken: 2");
	}

	/*
	 * Test of graph05.gka
	 */
	@Test
	public void testgraph05() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\graph05.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		BreadthFirst bfs = new BreadthFirst(nodes, edges);

		assertEquals(bfs.traverse("v1", "v5"), "v1 > v5 Steps taken: 1");
		assertEquals(bfs.traverse("v2", "v3"), "v2 > v3 Steps taken: 1");
		assertEquals(bfs.traverse("v4", "v2"), "v4 > v2 Steps taken: 1");
		assertEquals(bfs.traverse("v5", "v1"), "v5 > v1 Steps taken: 1");
	}

	/*
	 * Test of graph06.gka
	 */
	@Test
	public void testgraph06() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\graph06.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		BreadthFirst bfs = new BreadthFirst(nodes, edges);

		assertEquals(bfs.traverse("5", "10"), "No valid path found");
		assertEquals(bfs.traverse("5", "3"), "5 > 2 > 4 > 1 > 3 Steps taken: 4");
		assertEquals(bfs.traverse("3", "6"), "3 > 4 > 1 > 7 > 9 > 6 Steps taken: 5");
		assertEquals(bfs.traverse("12", "11"), "No valid path found");
	}

	/*
	 * Test of graph07.gka
	 */
	@Test
	public void testgraph07() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\graph07.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		BreadthFirst bfs = new BreadthFirst(nodes, edges);

		assertEquals(bfs.traverse("q", "s"), "q > s Steps taken: 1");
		assertEquals(bfs.traverse("v4", "v3"), "v4 > s > v3 Steps taken: 2");
		assertEquals(bfs.traverse("v1", "v2"), "v1 > v4 > v2 Steps taken: 2");
		assertEquals(bfs.traverse("v8", "v5"), "v8 > v2 > v5 Steps taken: 2");
	}

	/*
	 * Test of graph08.gka
	 */
	@Test
	public void testgraph08() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\graph08.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		BreadthFirst bfs = new BreadthFirst(nodes, edges);

		assertEquals(bfs.traverse("v12", "v14"), "v12 > v3 > v1 > v2 > v5 > v14 Steps taken: 5");
		assertEquals(bfs.traverse("v13", "v16"), "v13 > v4 > v1 > v2 > v6 > v16 Steps taken: 5");
		assertEquals(bfs.traverse("v7", "v6"), "v7 > v2 > v6 Steps taken: 2");
		assertEquals(bfs.traverse("v8", "v7"), "v8 > v2 > v7 Steps taken: 2");
	}

	/*
	 * Test of graph09.gka
	 */
	@Test
	public void testgraph09() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\graph09.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		BreadthFirst bfs = new BreadthFirst(nodes, edges);

		assertEquals(bfs.traverse("j", "k"), "j > k Steps taken: 1");
		assertEquals(bfs.traverse("h", "k"), "h > c > k Steps taken: 2");
		assertEquals(bfs.traverse("a", "h"), "a > f > h Steps taken: 2");
		assertEquals(bfs.traverse("c", "g"), "c > g Steps taken: 1");
	}

	/*
	 * Test of graph10.gka
	 */
	@Test
	public void testgraph10() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\graph10.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		BreadthFirst bfs = new BreadthFirst(nodes, edges);

		assertEquals(bfs.traverse("v1", "v10"), "v1 > v7 > v8 > v10 Steps taken: 3");
		assertEquals(bfs.traverse("v3", "q"), "v3 > v4 > q Steps taken: 2");
		assertEquals(bfs.traverse("s", "v5"), "s > q > v5 Steps taken: 2");
		assertEquals(bfs.traverse("v2", "v7"), "v2 > v4 > v7 Steps taken: 2");
	}
}
