package test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

import org.junit.Test;

import algorithms.BreadthFirst;
import algorithms.Dijkstra;
import algorithms.FloydWarshall;
import components.Edge;
import components.Node;
import io.DataHandler;

public class TestGraphBigDijFloyd {

	/*
	 * Test of graph01.gka
	 */
	@Test
	public void testgraph01() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\BIG.gka");

		HashMap<String, Node> nodes = dataHandler.getNodes();
		Set<Edge> edges = dataHandler.getEdges();
		FloydWarshall fw = new FloydWarshall(nodes, edges);
		Dijkstra d = new Dijkstra(nodes,edges);
		fw.traverse("1", "3");
		d.traverse("1", "3");
		assertEquals(d.getFinalPath(),fw.getFinalPath());
		assertEquals(fw.getFinalPath(), new LinkedList<Node>(Arrays.asList(nodes.get("1"),nodes.get("2"),nodes.get("13"),nodes.get("92"),nodes.get("3"))));
		fw.traverse("3", "8");
		d.traverse("3", "8");
		assertEquals(d.getFinalPath(),fw.getFinalPath());
		assertEquals(fw.getFinalPath(), new LinkedList<Node>(Arrays.asList(nodes.get("3"),nodes.get("49"),nodes.get("67"),nodes.get("8"))));
		fw.traverse("3", "1");
		d.traverse("3", "1");
		assertEquals(d.getFinalPath(),fw.getFinalPath());
		assertEquals(fw.getFinalPath(), new LinkedList<Node>(Arrays.asList(nodes.get("3"),nodes.get("49"),nodes.get("67"),nodes.get("1"))));
		fw.traverse("65", "2");
		d.traverse("65", "2");
		assertEquals(d.getFinalPath(),fw.getFinalPath());
		assertEquals(fw.getFinalPath(), new LinkedList<Node>(Arrays.asList(nodes.get("65"),nodes.get("40"),nodes.get("82"),nodes.get("20"),nodes.get("2"))));
	}
}
