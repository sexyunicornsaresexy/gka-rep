package test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import algorithms.BreadthFirst;
import components.Edge;
import components.Node;
import io.DataHandler;

public class TestDataHandler {
	/*
	 * Test of undirected loading
	 */
	@Test
	public void testLoadUndirected() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\loadtest1.gka");
			
			
		
		Node node1 = new Node("a");
		Node node2 = new Node("b");
		Node node3 = new Node("c");
		Node node4 = new Node("d");
		Node node5 = new Node("e");
		Node node6 = new Node("f");

		Edge edge1 = new Edge(node1, node2, false, null, null);
		Edge edge2 = new Edge(node2, node3, false, "test", null);
		Edge edge3 = new Edge(node3, node4, false, "name", 123);
		Edge edge4 = new Edge(node4, node5, false, null, 321);

		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);
		node2.addNeighbor(edge2);
		node3.addNeighbor(edge2);
		node3.addNeighbor(edge3);
		node4.addNeighbor(edge3);
		node4.addNeighbor(edge4);
		node5.addNeighbor(edge4);

		HashMap<String, Node> createdNodes = new HashMap<String, Node>();
		Set<Edge> createdEdges = new HashSet<Edge>();

		createdNodes.put("a", node1);
		createdNodes.put("b", node2);
		createdNodes.put("c", node3);
		createdNodes.put("d", node4);
		createdNodes.put("e", node5);
		createdNodes.put("f", node6);

		createdEdges.add(edge1);
		createdEdges.add(edge2);
		createdEdges.add(edge3);
		createdEdges.add(edge4);

		HashMap<String, Node> loadedNodes = dataHandler.getNodes();
		Set<Edge> loadedEdges = dataHandler.getEdges();

		assertEquals(createdNodes, loadedNodes);
		assertEquals(createdEdges, loadedEdges);

	}

	/*
	 * Test of directed loading
	 */
	@Test
	public void testLoadDirected() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\loadtest2.gka");

		Node node1 = new Node("a");
		Node node2 = new Node("b");
		Node node3 = new Node("c");
		Node node4 = new Node("d");
		Node node5 = new Node("e");
		Node node6 = new Node("f");

		Edge edge1 = new Edge(node1, node2, true, null, null);
		Edge edge2 = new Edge(node2, node3, true, "test", null);
		Edge edge3 = new Edge(node3, node4, true, "name", 123);
		Edge edge4 = new Edge(node4, node5, true, null, 321);

		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);
		node2.addNeighbor(edge2);
		node3.addNeighbor(edge2);
		node3.addNeighbor(edge3);
		node4.addNeighbor(edge3);
		node4.addNeighbor(edge4);
		node5.addNeighbor(edge4);

		HashMap<String, Node> createdNodes = new HashMap<String, Node>();
		Set<Edge> createdEdges = new HashSet<Edge>();

		createdNodes.put("a", node1);
		createdNodes.put("b", node2);
		createdNodes.put("c", node3);
		createdNodes.put("d", node4);
		createdNodes.put("e", node5);
		createdNodes.put("f", node6);

		createdEdges.add(edge1);
		createdEdges.add(edge2);
		createdEdges.add(edge3);
		createdEdges.add(edge4);

		HashMap<String, Node> loadedNodes = dataHandler.getNodes();
		Set<Edge> loadedEdges = dataHandler.getEdges();

		assertEquals(createdNodes, loadedNodes);
		assertEquals(createdEdges, loadedEdges);

	}

	/*
	 * Test of the saving
	 */
	@Test
	public void testSave() {

		DataHandler dataHandler = new DataHandler();
		dataHandler.load(System.getProperty("user.dir") + "\\graphs\\loadTest1.gka");

		dataHandler.save(new File(System.getProperty("user.dir") + "\\graphs\\saveTest1.gka"), dataHandler);
		DataHandler dataHandler2 = new DataHandler();
		dataHandler2.load(System.getProperty("user.dir") + "\\graphs\\saveTest1.gka");

		assertEquals(dataHandler.getNodes(), dataHandler2.getNodes());
		assertEquals(dataHandler.getEdges(), dataHandler2.getEdges());
	}
}