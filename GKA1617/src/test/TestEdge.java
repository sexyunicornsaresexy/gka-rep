package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import components.Edge;
import components.Node;

public class TestEdge {

	/*
	 * Test of retrieving Nodes from edge
	 */
	@Test
	public void testNodeRetrieval() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Node node3 = new Node("node3");

		Edge edge1 = new Edge(node1, node2, false, null, null);
		Edge edge2 = new Edge(node2, node3, false, null, null);

		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);
		node2.addNeighbor(edge2);
		node3.addNeighbor(edge2);

		assertEquals(edge1.getOne(), node1);
		assertEquals(edge1.getTwo(), node2);

		assertEquals(edge2.getOne(), node2);
		assertEquals(edge2.getTwo(), node3);

	}

	/*
	 * Test of getDirected() function
	 */
	@Test
	public void testGetDirected() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Edge edge1 = new Edge(node1, node2, false, null, null);
		Edge edge2 = new Edge(node2, node1, true, null, null);
		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);

		assertEquals(edge1.getDirected(), false);
		assertEquals(edge2.getDirected(), true);
	}

	/*
	 * Test of weight functions
	 */
	@Test
	public void testWeight() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Edge edge1 = new Edge(node1, node2, false, null, null);
		Edge edge2 = new Edge(node2, node1, true, null, 8);
		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);
		node1.addNeighbor(edge2);
		node2.addNeighbor(edge2);

		assertNull(edge1.getWeight());

		edge1.setWeight(5);

		assert (edge1.getWeight() == 5);
		assert (edge2.getWeight() == 8);
		assert (edge1.compareTo(edge2) == -3);
		assert (edge2.compareTo(edge1) == 3);
		edge1.setWeight(2);
		edge2.setWeight(9);

		assert (edge1.getWeight() == 2);
		assert (edge2.getWeight() == 9);
		assert (edge1.compareTo(edge2) == -7);
		assert (edge2.compareTo(edge1) == 7);
	}

	/*
	 * Test of name functions
	 */
	@Test
	public void testName() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Edge edge1 = new Edge(node1, node2, false, null, null);
		Edge edge2 = new Edge(node2, node1, true, "testname", null);
		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);
		node1.addNeighbor(edge2);
		node2.addNeighbor(edge2);

		assertEquals(edge1.getName(), "node1node2");
		assertEquals(edge1.getWasnamed(), false);

		assertEquals(edge2.getName(), "testname");
		assertEquals(edge2.getWasnamed(), true);
	}

	/*
	 * Test of getNeighbor() function
	 */
	@Test
	public void testGetNeighbor() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Node node3 = new Node("node3");

		Edge edge1 = new Edge(node1, node2, false, null, null);
		Edge edge2 = new Edge(node2, node3, false, null, null);

		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);
		node2.addNeighbor(edge2);
		node3.addNeighbor(edge2);

		assertEquals(edge1.getNeighbor(node1), node2);
		assertEquals(edge1.getNeighbor(node2), node1);

		assertEquals(edge2.getNeighbor(node2), node3);
		assertEquals(edge2.getNeighbor(node3), node2);

	}

	/*
	 * Test of toString() function
	 */
	@Test
	public void testToString() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Edge edge1 = new Edge(node1, node2, false, "test", null);
		Edge edge2 = new Edge(node2, node1, true, null, 5);
		Edge edge3 = new Edge(node1, node2, false, "name", 9);
		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);

		assertEquals(edge1.toString(), "node1 -- node2 (test);");
		assertEquals(edge2.toString(), "node2 -> node1 : 5;");
		assertEquals(edge3.toString(), "node1 -- node2 (name) : 9;");

	}

	/*
	 * Test of equality
	 */
	@Test
	public void testEqual() {
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Edge edge1 = new Edge(node1, node2, false, null, null);
		Edge edge2 = new Edge(node2, node1, true, null, null);
		Edge edge3 = new Edge(node1, node2, false, null, null);
		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);

		assertEquals(edge1.equals(new Object()), false);
		assertEquals(edge2.equals(edge2), true);
		assertEquals(edge1.equals(edge2), false);
		assertEquals(edge1.equals(edge3), true);
	}

}
