package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import algorithms.BreadthFirst;
import algorithms.Dijkstra;
import algorithms.FloydWarshall;
import components.Edge;
import components.Node;

public class TestDijkstra {

	/*
	 * Test of directed graphs
	 */
	@Test
	public void testDirectedGraph() {

		HashMap<String, Node> nodes = new HashMap<String, Node>();
		Set<Edge> edges = new HashSet<Edge>();
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Node node3 = new Node("node3");
		Node node4 = new Node("node4");
		Node node5 = new Node("node5");
		Node unreachable = new Node("unreachable");

		nodes.put("node1", node1);
		nodes.put("node2", node2);
		nodes.put("node3", node3);
		nodes.put("node4", node4);
		nodes.put("node5", node5);
		nodes.put("unreachable", unreachable);

		Edge edge1 = new Edge(node1, node3, true, null, 1);
		Edge edge2 = new Edge(node3, node5, true, null, 2);
		Edge edge3 = new Edge(node2, node4, true, null, 3);
		Edge edge4 = new Edge(node4, node5, true, null, 4);
		Edge edge5 = new Edge(node1, node2, true, null, 4);

		node1.addNeighbor(edge1);
		node3.addNeighbor(edge1);
		node3.addNeighbor(edge2);
		node5.addNeighbor(edge2);
		node2.addNeighbor(edge3);
		node4.addNeighbor(edge3);
		node4.addNeighbor(edge4);
		node5.addNeighbor(edge4);
		node1.addNeighbor(edge5);
		node2.addNeighbor(edge5);
		
		edges.add(edge1);
		edges.add(edge2);
		edges.add(edge3);
		edges.add(edge4);
		edges.add(edge5);

		Dijkstra d = new Dijkstra(nodes, edges);
		d.traverse("node1", "node5");	

		assertEquals(d.getFinalPath(),new LinkedList<Node>(Arrays.asList(node1,node3,node5)));
		d.traverse("node1", "unreachable");
		assertEquals(d.getFinalPath(),new LinkedList<Node>(Arrays.asList()));
		d.traverse("node1", "node2");
		assertEquals(d.getFinalPath(),new LinkedList<Node>(Arrays.asList(node1,node2)));		
		d.traverse("node2", "node5");
		assertEquals(d.getFinalPath(),new LinkedList<Node>(Arrays.asList(node2,node4,node5)));		
		d.traverse("node1", "node1");
		assertEquals(d.getFinalPath(),new LinkedList<Node>(Arrays.asList()));		
		assertEquals(d.traverse("node123", "node321"), "Neither node was found.");
		assertEquals(d.traverse("node1", "node123"), "target node was not found.");
		assertEquals(d.traverse("node123", "node1"), "source node was not found.");
	}

	/*
	 * Test of undirected graphs
	 */
	@Test
	public void testUndirectedGraph() {

		HashMap<String, Node> nodes = new HashMap<String, Node>();
		Set<Edge> edges = new HashSet<Edge>();
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");
		Node node3 = new Node("node3");
		Node node4 = new Node("node4");
		Node node5 = new Node("node5");
		Node unreachable = new Node("unreachable");

		nodes.put("node1", node1);
		nodes.put("node2", node2);
		nodes.put("node3", node3);
		nodes.put("node4", node4);
		nodes.put("node5", node5);
		nodes.put("unreachable", unreachable);

		Edge edge1 = new Edge(node1, node5, false, null, 5);
		Edge edge2 = new Edge(node1, node3, false, null, 1);
		Edge edge3 = new Edge(node3, node4, false, null, 1);
		Edge edge4 = new Edge(node4, node5, false, null, 1);

		node1.addNeighbor(edge1);
		node5.addNeighbor(edge1);
		node1.addNeighbor(edge2);
		node3.addNeighbor(edge2);
		node3.addNeighbor(edge3);
		node4.addNeighbor(edge3);
		node4.addNeighbor(edge4);
		node5.addNeighbor(edge4);

		edges.add(edge1);
		edges.add(edge2);
		edges.add(edge3);
		edges.add(edge4);

		Dijkstra d = new Dijkstra(nodes, edges);
		d.traverse("node1", "node5");
		assertEquals(d.getFinalPath(), new LinkedList<Node>(Arrays.asList(node1,node3,node4,node5)));
		d.traverse("node1", "node2");
		assertEquals(d.getFinalPath(), new LinkedList<Node>(Arrays.asList()));
		d.traverse("node5", "node1");
		assertEquals(d.getFinalPath(), new LinkedList<Node>(Arrays.asList(node5,node4,node3,node1)));
		d.traverse("node1", "unreachable");
		assertEquals(d.getFinalPath(), new LinkedList<Node>(Arrays.asList()));

		assertEquals(d.traverse("node1", "node1"), "Source equals target");
		assertEquals(d.traverse("node123", "node321"), "Neither node was found.");
		assertEquals(d.traverse("node1", "node123"), "target node was not found.");
		assertEquals(d.traverse("node123", "node1"), "source node was not found.");
	}

	/*
	 * Test of undirected graphs
	 */
	@Test
	public void testUndirectedGraphNotExistingSource() {
		HashMap<String, Node> nodes = new HashMap<String, Node>();
		Set<Edge> edges = new HashSet<Edge>();
		Node node1 = new Node("node1");
		Node node2 = new Node("node2");

		nodes.put("node1", node1);
		nodes.put("node2", node2);

		Edge edge1 = new Edge(node1, node2, false, null, null);

		node1.addNeighbor(edge1);
		node2.addNeighbor(edge1);

		edges.add(edge1);

		Dijkstra d = new Dijkstra(nodes, edges);
		assertEquals(d.traverse("node123", "node1"), "source node was not found.");
	}

}
