/**
 * This is the main gui class 
 * @author Alexander Reichart, Andrada Boldis
 * @date October 2016
 */

package gui;

import java.awt.Component;
import java.awt.Dimension;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JButton;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import javax.swing.JTextField;

import algorithms.*;
import components.Edge;
import components.Node;
import io.DataHandler;
import io.GraphGenerator;

import org.graphstream.*;
import org.graphstream.ui.*;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerPipe;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {

	public MainWindow() {

		initUI();
	}

	private void initUI() {
		createMenuBar();
		setTitle("Main Window");
		setSize(300, 200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	private void createMenuBar() {
		final JFileChooser fc = new JFileChooser();
		DataHandler dataHandler = new DataHandler();

		JMenuBar menubar = new JMenuBar();

		JMenu file = new JMenu("File");
		file.setMnemonic(KeyEvent.VK_F);


		
		JMenu algorithms = new JMenu("Algorithms");
		algorithms.setMnemonic(KeyEvent.VK_A);
		algorithms.setEnabled(false);

		JMenuItem lMenuItem = new JMenuItem("Load");
		lMenuItem.setMnemonic(KeyEvent.VK_L);
		lMenuItem.setToolTipText("Load Graph");

		JMenuItem sMenuItem = new JMenuItem("Save");
		sMenuItem.setMnemonic(KeyEvent.VK_S);
		sMenuItem.setToolTipText("Save Graph");
		
		JMenuItem generate = new JMenuItem("Generate");
		generate.setMnemonic(KeyEvent.VK_G);
		

		JMenuItem bfsMenuItem = new JMenuItem("BFS");
		bfsMenuItem.setMnemonic(KeyEvent.VK_B);
		bfsMenuItem.setToolTipText("Breadth First Search");
		
		JMenuItem dijMenuItem = new JMenuItem("Dijkstra");
		dijMenuItem.setMnemonic(KeyEvent.VK_D);
		dijMenuItem.setToolTipText("Dijkstra");
		
		JMenuItem fwMenuItem = new JMenuItem("Floyd Warshall");
		dijMenuItem.setMnemonic(KeyEvent.VK_W);
		dijMenuItem.setToolTipText("Floyd Warshall");
		
		JMenuItem ffMenuItem = new JMenuItem("Ford Fulkerson");
		dijMenuItem.setMnemonic(KeyEvent.VK_O);
		dijMenuItem.setToolTipText("Ford Fulkerson");
		
		JMenuItem ekMenuItem = new JMenuItem("Edmond Karp");
		dijMenuItem.setMnemonic(KeyEvent.VK_E);
		dijMenuItem.setToolTipText("Edmond and Karp");
		
		lMenuItem.addActionListener((ActionEvent event) -> {
			String fileName = "";
			if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				fileName = fc.getSelectedFile() + "";
				Boolean loaded = dataHandler.load(fileName);
				if (loaded) {
					getContentPane().removeAll();
					Graph graph2 = new SingleGraph("test");
					graph2.addAttribute("ui.stylesheet","graph {fill-color:#DDDDFF;} node {text-size:30px;} edge {text-size:15px;}");					
					if (dataHandler.getDirected()) {
						

						for (Node node : dataHandler.getNodes().values()) {
							if (!graph2.hasLabel(node.getLabel()))
								graph2.addNode(node.getLabel()).setAttribute("ui.label",node.getLabel());

						}
						
//						dataHandler.getEdges().forEach(e -> graph2.addEdge(e.getName(), e.getOne().getLabel(), e.getTwo().getLabel(),true).setAttribute("ui.label",e.getWeight()));
for(Edge e : dataHandler.getEdges()){
try{
	graph2.addEdge(e.getName(), e.getOne().getLabel(), e.getTwo().getLabel(),true).setAttribute("ui.label",e.getWeight());	
}finally{}

}

					} else {

//						dataHandler.getEdges().forEach(e -> graph.addEdge(e, e.getOne(), e.getTwo()));

/*						for (Node node : dataHandler.getNodes().values()) {
							if (!graph.containsVertex(node))
								graph.addVertex(node);
						}
*/						
						for (Node node : dataHandler.getNodes().values()) {
							if (!graph2.hasLabel(node.getLabel()))
								graph2.addNode(node.getLabel()).setAttribute("ui.label",node.getLabel());

						}
//						dataHandler.getEdges().forEach(e -> graph2.addEdge(e.getName(), e.getOne().getLabel(), e.getTwo().getLabel(),false).setAttribute("ui.label",e.getWeight()));
						for(Edge e : dataHandler.getEdges()){
							try{
								graph2.addEdge(e.getName(), e.getOne().getLabel(), e.getTwo().getLabel(),true).setAttribute("ui.label",e.getWeight());	
							}finally{}					
						}
//graph2.display();												



//						this.getContentPane().add(vs);
					}
					Viewer viewer = new Viewer(graph2, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
					 viewer.enableAutoLayout();
				       View view = viewer.addDefaultView(false);

				        ViewerPipe fromViewer = viewer.newViewerPipe();
				        fromViewer.addSink(graph2);


				       
					this.getContentPane().add((Component) view);						
					this.setSize(800, 800);
					this.setVisible(true);
					algorithms.setEnabled(true);
//					graph2.display();					
				
				}
			} else {
				System.out.println("No Selection ");
			}

		});

		sMenuItem.addActionListener((ActionEvent event) -> {

			int returnVal = fc.showSaveDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File selectedFile = fc.getSelectedFile();
				DataHandler.save(selectedFile, dataHandler);
			}

		});

		bfsMenuItem.addActionListener((ActionEvent event) -> {

			BreadthFirst algorithm = new BreadthFirst(dataHandler.getNodes(), dataHandler.getEdges());
			JFrame dialog = new JFrame(); // parent, isModal
			dialog.setLayout(new FlowLayout());
			dialog.setResizable(false);
			dialog.setSize(200, 220);
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);

			JLabel sourceLabel = new JLabel("source", JLabel.TRAILING);
			sourceLabel.setPreferredSize(new Dimension(40, 40));
			dialog.add(sourceLabel);
			JTextField sourceTextField = new JTextField(10);
			sourceTextField.setPreferredSize(new Dimension(40, 40));
			sourceLabel.setLabelFor(sourceTextField);
			dialog.add(sourceTextField);

			JLabel targetLabel = new JLabel("target", JLabel.TRAILING);
			targetLabel.setPreferredSize(new Dimension(40, 40));
			dialog.add(targetLabel);
			JTextField targetTextField = new JTextField(10);
			targetTextField.setPreferredSize(new Dimension(40, 40));
			targetLabel.setLabelFor(targetTextField);
			dialog.add(targetTextField);
			JLabel output = new JLabel("", JLabel.LEADING);
			output.setPreferredSize(new Dimension(190, 40));

			JButton button = new JButton("Submit");
			button.setPreferredSize(new Dimension(190, 40));
			dialog.add(button);
			dialog.add(output);
			button.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					output.setText("<html>" + algorithm.traverse(sourceTextField.getText(), targetTextField.getText())
							+ "</html>");
				}
			});

		});
		
		generate.addActionListener((ActionEvent event) -> {

			
			JFrame dialog = new JFrame(); // parent, isModal
			dialog.setLayout(new FlowLayout());
			dialog.setResizable(false);
			dialog.setSize(300, 220);
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);

			JLabel nodesLabel = new JLabel("Number of nodes", JLabel.TRAILING);
			nodesLabel.setPreferredSize(new Dimension(110, 40));
			dialog.add(nodesLabel);
			JTextField nodesTextField = new JTextField(10);
			nodesTextField.setPreferredSize(new Dimension(20, 40));
			nodesLabel.setLabelFor(nodesTextField);
			dialog.add(nodesTextField);

			JLabel edgesLabel = new JLabel("Number of edges", JLabel.TRAILING);
			edgesLabel.setPreferredSize(new Dimension(110, 40));
			dialog.add(edgesLabel);
			JTextField edgesTextField = new JTextField(10);
			edgesTextField.setPreferredSize(new Dimension(20, 40));
			edgesLabel.setLabelFor(edgesTextField);
			dialog.add(edgesTextField);
			JLabel output = new JLabel("", JLabel.LEADING);
			output.setPreferredSize(new Dimension(190, 40));

			JButton button = new JButton("Generate");
			button.setPreferredSize(new Dimension(200, 40));
			dialog.add(button);
			dialog.add(output);
			button.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					if (nodesTextField.getText().equals("") || edgesTextField.getText().equals("") ){
						output.setText("<html>" + "Please input the number of nodes and edges." + "</html>"); 
					}
					else
						{
						Integer nodesNr = Integer.parseInt(nodesTextField.getText());
						Integer edgesNr = Integer.parseInt(edgesTextField.getText());
						if ( edgesNr > nodesNr* (nodesNr-1)){
							output.setText("<html>" + "Too many edges. Please input a valid number." + "</html>");
						}
	 					else{
							GraphGenerator graph = new GraphGenerator ();
							graph.generateDirectedGraph(nodesNr, edgesNr);
							output.setText("<html>" + "Graph has been successfully generated" + "</html>");
						}
					}							
				}
			});

		});
		
		
		dijMenuItem.addActionListener((ActionEvent event) -> {

			Dijkstra algorithm = new Dijkstra(dataHandler.getNodes(), dataHandler.getEdges());
			JFrame dialog = new JFrame(); // parent, isModal
			dialog.setLayout(new FlowLayout());
			dialog.setResizable(false);
			dialog.setSize(200, 320);
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);

			JLabel sourceLabel = new JLabel("source", JLabel.TRAILING);
			sourceLabel.setPreferredSize(new Dimension(40, 40));
			dialog.add(sourceLabel);
			JTextField sourceTextField = new JTextField(10);
			sourceTextField.setPreferredSize(new Dimension(40, 40));
			sourceLabel.setLabelFor(sourceTextField);
			dialog.add(sourceTextField);

			JLabel targetLabel = new JLabel("target", JLabel.TRAILING);
			targetLabel.setPreferredSize(new Dimension(40, 40));
			dialog.add(targetLabel);
			JTextField targetTextField = new JTextField(10);
			targetTextField.setPreferredSize(new Dimension(40, 40));
			targetLabel.setLabelFor(targetTextField);
			dialog.add(targetTextField);
			JLabel output = new JLabel("", JLabel.LEADING);
			output.setPreferredSize(new Dimension(190, 140));

			JButton button = new JButton("Submit");
			button.setPreferredSize(new Dimension(190, 40));
			dialog.add(button);
			dialog.add(output);
			button.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					output.setText("<html>" + 
				algorithm.traverse(sourceTextField.getText(), targetTextField.getText())
							+ "</html>");

				}
			});

		});
		
		
		fwMenuItem.addActionListener((ActionEvent event) -> {

			FloydWarshall algorithm = new FloydWarshall(dataHandler.getNodes(), dataHandler.getEdges());
			JFrame dialog = new JFrame(); // parent, isModal
			dialog.setLayout(new FlowLayout());
			dialog.setResizable(false);
			dialog.setSize(200, 320);
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);

			JLabel sourceLabel = new JLabel("source", JLabel.TRAILING);
			sourceLabel.setPreferredSize(new Dimension(40, 40));
			dialog.add(sourceLabel);
			JTextField sourceTextField = new JTextField(10);
			sourceTextField.setPreferredSize(new Dimension(40, 40));
			sourceLabel.setLabelFor(sourceTextField);
			dialog.add(sourceTextField);

			JLabel targetLabel = new JLabel("target", JLabel.TRAILING);
			targetLabel.setPreferredSize(new Dimension(40, 40));
			dialog.add(targetLabel);
			JTextField targetTextField = new JTextField(10);
			targetTextField.setPreferredSize(new Dimension(40, 40));
			targetLabel.setLabelFor(targetTextField);
			dialog.add(targetTextField);
			JLabel output = new JLabel("", JLabel.LEADING);
			output.setPreferredSize(new Dimension(190, 140));

			JButton button = new JButton("Submit");
			button.setPreferredSize(new Dimension(190, 40));
			dialog.add(button);
			dialog.add(output);
			button.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					output.setText("<html>" + 
				algorithm.traverse(sourceTextField.getText(), targetTextField.getText())
							+ "</html>");

				}
			});

		});
		

		ffMenuItem.addActionListener((ActionEvent event) -> {

			FordFulkerson algorithm = new FordFulkerson(dataHandler.getNodes(), dataHandler.getEdges());
			JFrame dialog = new JFrame(); // parent, isModal
			dialog.setLayout(new FlowLayout());
			dialog.setResizable(false);
			dialog.setSize(200, 320);
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);

			JLabel sourceLabel = new JLabel("source", JLabel.TRAILING);
			sourceLabel.setPreferredSize(new Dimension(40, 40));
			dialog.add(sourceLabel);
			JTextField sourceTextField = new JTextField(10);
			sourceTextField.setPreferredSize(new Dimension(40, 40));
			sourceLabel.setLabelFor(sourceTextField);
			dialog.add(sourceTextField);

			JLabel targetLabel = new JLabel("target", JLabel.TRAILING);
			targetLabel.setPreferredSize(new Dimension(40, 40));
			dialog.add(targetLabel);
			JTextField targetTextField = new JTextField(10);
			targetTextField.setPreferredSize(new Dimension(40, 40));
			targetLabel.setLabelFor(targetTextField);
			dialog.add(targetTextField);
			JLabel outputField = new JLabel("", JLabel.LEADING);
			outputField.setPreferredSize(new Dimension(190, 140));

			JButton button = new JButton("Submit");
			button.setPreferredSize(new Dimension(190, 40));
			dialog.add(button);
			dialog.add(outputField);
			button.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					String output="";
					String source = sourceTextField.getText();
					String target = targetTextField.getText();
					if (source.equals(target))
						output = "Source equals target";

					if (!dataHandler.getNodes().containsKey(source) & !dataHandler.getNodes().containsKey(target)) {
						output = "Neither node was found.";
					} else if (!dataHandler.getNodes().containsKey(source) & dataHandler.getNodes().containsKey(target)) {
						output = "source node was not found.";
					} else if (dataHandler.getNodes().containsKey(source) & !dataHandler.getNodes().containsKey(target)) {
						output = "target node was not found.";
					}
					output = String.valueOf(algorithm.traverse(dataHandler.getNodes().get(source), dataHandler.getNodes().get(target)));
					
					outputField.setText("<html> Max Flow: " + 
							output + "<br>" + String.valueOf(algorithm.getTime())
							+ "ms</html>");

				}
			});

		});
		
		
		ekMenuItem.addActionListener((ActionEvent event) -> {

			EdmondKarp algorithm = new EdmondKarp(dataHandler.getNodes(), dataHandler.getEdges());
			JFrame dialog = new JFrame(); // parent, isModal
			dialog.setLayout(new FlowLayout());
			dialog.setResizable(false);
			dialog.setSize(200, 320);
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);

			JLabel sourceLabel = new JLabel("source", JLabel.TRAILING);
			sourceLabel.setPreferredSize(new Dimension(40, 40));
			dialog.add(sourceLabel);
			JTextField sourceTextField = new JTextField(10);
			sourceTextField.setPreferredSize(new Dimension(40, 40));
			sourceLabel.setLabelFor(sourceTextField);
			dialog.add(sourceTextField);

			JLabel targetLabel = new JLabel("target", JLabel.TRAILING);
			targetLabel.setPreferredSize(new Dimension(40, 40));
			dialog.add(targetLabel);
			JTextField targetTextField = new JTextField(10);
			targetTextField.setPreferredSize(new Dimension(40, 40));
			targetLabel.setLabelFor(targetTextField);
			dialog.add(targetTextField);
			JLabel outputField = new JLabel("", JLabel.LEADING);
			outputField.setPreferredSize(new Dimension(190, 140));

			JButton button = new JButton("Submit");
			button.setPreferredSize(new Dimension(190, 40));
			dialog.add(button);
			dialog.add(outputField);
			button.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					String output="";
					String source = sourceTextField.getText();
					String target = targetTextField.getText();
					if (source.equals(target))
						output = "Source equals target";

					if (!dataHandler.getNodes().containsKey(source) & !dataHandler.getNodes().containsKey(target)) {
						output = "Neither node was found.";
					} else if (!dataHandler.getNodes().containsKey(source) & dataHandler.getNodes().containsKey(target)) {
						output = "source node was not found.";
					} else if (dataHandler.getNodes().containsKey(source) & !dataHandler.getNodes().containsKey(target)) {
						output = "target node was not found.";
					}
					output = String.valueOf(algorithm.traverse(dataHandler.getNodes().get(source), dataHandler.getNodes().get(target)));
					
					outputField.setText("<html> Max Flow: " + 
							output + "<br>" + String.valueOf(algorithm.getTime())
							+ "ms</html>");

				}
			});

		});
		
		file.add(lMenuItem);
		file.add(sMenuItem);
		file.add(generate);
		algorithms.add(bfsMenuItem);
		algorithms.add(dijMenuItem);
		algorithms.add(fwMenuItem);		
		algorithms.add(ffMenuItem);	
		algorithms.add(ekMenuItem);	

		menubar.add(file);
		menubar.add(algorithms);


		setJMenuBar(menubar);
	}

	public static void main(String[] args) {

		EventQueue.invokeLater(() -> {

			MainWindow ex = new MainWindow();
			ex.setVisible(true);
		});
		


	}
}