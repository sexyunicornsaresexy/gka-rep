/**
 * This class is used for the edges of the graphs. Both directed and undirected.
 * 
 * @author Alexander Reichart, Andrada Boldis
 * @date October 2016
 */
package algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import components.Edge;
import components.Node;

public class Dijkstra {

	HashMap<String, Node> nodes;
	List<Edge> edges = new ArrayList<Edge>();
	HashMap<Node, Integer> values = new HashMap<Node, Integer>();
	HashMap<Node, Node> predecessor = new HashMap<Node, Node>();

	Queue<Node> queue = new LinkedList<Node>();
	LinkedList<Node> finalPath = new LinkedList<Node>(); 
	
	public Dijkstra(HashMap<String, Node> nodes, Set<Edge> edges) {
		this.nodes = nodes;
		this.edges.addAll(edges);
	}

	/**
	 * 
	 * @param source
	 *            label of start node
	 * @param target
	 *            label of end node
	 * @return String path from source to target
	 */

	public String traverse(String source, String target) {
		this.finalPath = new LinkedList<Node>(); 
		
		if (source.equals(target))
			return "Source equals target";

		if (!nodes.containsKey(source) & !nodes.containsKey(target)) {
			return "Neither node was found.";
		} else if (!nodes.containsKey(source) & nodes.containsKey(target)) {
			return "source node was not found.";
		} else if (nodes.containsKey(source) & !nodes.containsKey(target)) {
			return "target node was not found.";
		}

		Node startNode = nodes.get(source);
		this.values = new HashMap<Node, Integer>();
		this.values.put(startNode, 0);
		this.queue.add(startNode);

		while (!queue.isEmpty()) {
			Node currentNode = queue.poll();
			this.evaluateNeighbors(currentNode, this.values.get(currentNode));
		}
		traverseBack(this.nodes.get(source),this.nodes.get(target));
		


		if (finalPath.size()>0)
			return finalPath + "<br>Steps taken: " + finalPath.size()+ "<br>Total weight: " + values.get(nodes.get(target));
		else {
			return "No valid path found";
		}
	}

	/**
	 * 
	 * @param source
	 *            Node to start at
	 * @param path
	 *            String path so far
	 * @return String the final path
	 */
	public void traverseBack(Node source, Node target) {
		

		finalPath.add(0,target);
		do{			

			if(this.predecessor.containsKey(finalPath.get(0))){
			finalPath.add(0,this.predecessor.get(finalPath.get(0)));			
		}else{
			finalPath = new LinkedList<Node>();
			break;
		}
		}while(!finalPath.get(0).equals(source));
		System.out.println(finalPath);
	}

	/**
	 * 
	 * @param currentNode
	 *            node to evaluate and add to queue
	 * @param currentValue
	 *            value to assign to node
	 */
	public void evaluate(Node currentNode, Node previousNode, int currentValue) {

		if (!this.values.containsKey(currentNode)) {
			this.values.put(currentNode, currentValue);
			this.predecessor.put(currentNode, previousNode);
			queue.add(currentNode);
		}else{
			if(currentValue<this.values.get(currentNode)){
				this.values.put(currentNode, currentValue);				
				this.predecessor.put(currentNode, previousNode);
				queue.add(currentNode);
			}

		}

	}

	/**
	 * 
	 * @param currentNode
	 *            node whose neighbors we are going to evaluate
	 * @param currentValue
	 *            value of current node
	 */
	public void evaluateNeighbors(Node currentNode, int currentValue) {
		ArrayList<Edge> neighbors = currentNode.getNeighbors();

		for (Edge neighbor : neighbors) {
			neighbor.getWeight();
			if (neighbor.getDirected()) {
				if (neighbor.getOne().equals(currentNode) && !neighbor.getTwo().equals(currentNode))
					evaluate(neighbor.getTwo(),currentNode, currentValue + (neighbor.getWeight().equals(null) ? 1 : neighbor.getWeight()));
			} else {
				if (!neighbor.getOne().equals(neighbor.getTwo()))
				evaluate(neighbor.getNeighbor(currentNode),currentNode, currentValue + (neighbor.getWeight() == null ? 0 : neighbor.getWeight()));
			}

		}

	}

	public LinkedList<Node> getFinalPath() {
		return finalPath;
	}
}
