/**
 * This class is used for the edges of the graphs. Both directed and undirected.
 * 
 * @author Alexander Reichart, Andrada Boldis
 * @date October 2016
 */
package algorithms;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import components.Edge;
import components.Node;





/**
 * Class for marking of flow on nodes.
 * 
 *
 */
class Marking {
	
	// pos is true, when we came normally
	// from edge. when negative we came backwards.
	public Boolean	direction;
	
	// node we came from
	public Node	previous;
	
	public Integer flow;
	
	public String toString() {
		String directionString 	= (String) ((direction == null) ? "null" : direction.toString());
		String prev	= (String) ((previous == null) ? "null" : previous.toString());
		String flowString 	= (String) ((flow == null) ? "null" : flow.toString());
		return 	"{" + directionString + ", " + prev + ", " + flowString + "}"; 
	}
}

public class FordFulkerson {
	
	HashMap<String, Node> nodes;
	List<Edge> edges = new ArrayList<Edge>();
	
	private Map<Edge, Integer>edgeFlows;
	private Map<Node, Marking> markings;
	private	Set<Node> inspectedNodes;
	private double  time = 0;


	
	private void init() {
		inspectedNodes = new HashSet<>();
		edgeFlows = new HashMap<>();
		markings = new HashMap<>();
	}
	private void initEdgeFlow() {
		for(Edge edge : edges) {
			edgeFlows.put(edge, 0);
		}
	}
	
	/**
	 * 
	 * @param source
	 *            label of start node
	 * @param target
	 *            label of end node
	 * @return String path from source to target
	 */	
	public double traverse(Node source, Node target) {
		
		long startTime = System.nanoTime();
		init();
		Integer networkFlow = 0;
		// 1) initialization
		initEdgeFlow();
		Marking q_mark = new Marking();
		q_mark.flow = Integer.MAX_VALUE;
		q_mark.previous = null;
		q_mark.direction = null;
		markings.put(source, q_mark);
		
		// 2) inspect and mark
		while(!inspectedNodes.equals(markings.keySet())) {
			Node current;
			try {
				current = getUninspected();
			} catch (Exception e) {
				e.printStackTrace();
				return -1.0;
			}
			inspectedNodes.add(current);
			Boolean sink_is_marked = false;
			// forward edge
			for(Edge f_edge : current.getNeighbors()) {

				Node edge_source = f_edge.getOne();
				Node edge_target = f_edge.getTwo();
				Integer c = f_edge.getWeight();
				if(!(isMarked(edge_target)) && edgeFlows.get(f_edge) < c) {
					Marking m = new Marking();
					if(f_edge.getOne().equals(current)){
						m.direction 		= true;
						m.previous	= edge_source;						
					}else{
						m.direction 		= false;
						m.previous 	= edge_target;						
					}

					m.flow 		= (int) Math.min(c - edgeFlows.get(f_edge), deltaOf(edge_source));
					markings.put(edge_target, m);
					if(edge_target.equals(target)) 
						sink_is_marked = true;
				}
			}
			// 3) increase flow
			if(sink_is_marked) {
				Integer increment = markings.get(target).flow;
				increaseEdgeFlow(target, increment);
				networkFlow += increment;
				markings = new HashMap<>();
				inspectedNodes = new HashSet<>(); 
				markings.put(source, q_mark);
			}
		}
		long endTime = System.nanoTime();
		time = (endTime - startTime)/1e6;
		return networkFlow;
		
		
		
	}

	public FordFulkerson(HashMap<String, Node> nodes, Set<Edge> edges) {
		this.nodes = nodes;
		this.edges.addAll(edges);
		}

	private Node getUninspected() throws IllegalAccessException {
		Set<Node> marked = new HashSet<>();
		marked.addAll(markings.keySet());
		marked.removeAll(inspectedNodes);
		Iterator<Node> i = marked.iterator();
		if(!i.hasNext()) {
			throw new IllegalAccessException();
		}
		return i.next();
	}
	
	
	private Boolean isMarked(Node node) {
		return markings.keySet().contains(node);
	}
	
	/**
	 * 
	 * @param vertex
	 * @return current delta (flow) on vertex
	 */
	private Integer deltaOf(Node node) {
		Marking a = markings.get(node);
		return a.flow;
	}
	
	private void increaseEdgeFlow(Node node, Integer increment) {
		Marking m = markings.get(node);
		if(m.previous == null) {
			// pred is null at start vertex
			return;
		}
		// probably buggy: 	we should ensure that the edge is
		//					the correct one.

		Edge edge = m.previous.getConnection(node);
		if(m.direction) {
			// increase edge flow
			edgeFlows.put(edge, increment + edgeFlows.get(edge));			
		} else if(!m.direction) {
			// reduce edge flow
			edgeFlows.put(edge, edgeFlows.get(edge) - increment);
		}
		increaseEdgeFlow(m.previous, increment);
	}
	

	

public double getTime(){
	return time;
}


}
