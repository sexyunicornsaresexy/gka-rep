/**
 * This class is used for the edges of the graphs. Both directed and undirected.
 * 
 * @author Alexander Reichart, Andrada Boldis
 * @date October 2016
 */
package algorithms;



import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;


import components.Edge;
import components.Node;







public class EdmondKarp {
	
	private HashMap<String, Node> nodes;
	private ArrayList<Node> nodesList = new ArrayList<Node>();
	private List<Edge> edges = new ArrayList<Edge>();
	
	private Map<Edge,Integer> edgeFlows;
	private Map<Edge,Boolean> edgeDirection; //true = forward, false = backward
	private double												time;
	private Integer	maxFlow;
	private Map<Edge,Edge> 	predecessor;
	



	LinkedList<Node> finalPath = new LinkedList<Node>(); 
	
	public EdmondKarp(HashMap<String, Node> nodes, Set<Edge> edges) {
		this.nodes = nodes;
		this.edges.addAll(edges);
		}


	
	/**
	 * 
	 * @param source
	 *            label of start node
	 * @param target
	 *            label of end node
	 * @return String path from source to target
	 */	
	public double traverse(Node source, Node target) {
		long startTime = System.nanoTime();
		ArrayList<Edge> way = new ArrayList<>();
//		Set<Edge> all_edges = new HashSet<>();
//		all_edges = edges;
		edgeFlows = new HashMap<Edge, Integer>();
		edgeDirection = new HashMap<Edge, Boolean>();
		predecessor = new HashMap<Edge,Edge>();
		maxFlow = 0;
		//initialisierung des flows
//		for (NamedWeightedEdge e : all_edges) {
		for (Edge e : edges) {
			edgeFlows.put(e, 0);
		}
		//Wegfindung
		way = getWay(source,target);
		System.out.println(way.toString());		
		while (!way.isEmpty()) {
			updateFlow(way);
			way = getWay(source,target);
		}
		long endTime = System.nanoTime();
		time = (endTime - startTime)/1e6;

		return maxFlow;

}
	
	private void updateFlow(ArrayList<Edge> way) {
		Integer localFlow;
		Set<Integer> currentFlows = new HashSet<>();
		for (Edge e : way) {
			Integer capacity = e.getWeight();
			Integer curFlow = edgeFlows.get(e);
			if(edgeDirection.get(e)) {
				currentFlows.add(capacity-curFlow);
			} else {
				currentFlows.add(curFlow);
			}
		}
		localFlow = Collections.min(currentFlows);
		for (Edge e : way) {
			Integer capacity = e.getWeight();
			Integer curFlow = edgeFlows.get(e);
			if(edgeDirection.get(e)) {
				edgeFlows.put(e, curFlow+localFlow);
			} else {
				edgeFlows.put(e, curFlow-localFlow);
			}

			if (!((0.0 <= edgeFlows.get(e)) && (edgeFlows.get(e) <= capacity))) {
				try {
					throw new Exception();
				} catch (Exception e1) {
					System.out.println("Flow im inkonsistentem Zustand, Edge: " + e + "aktueller Flow:"
										+ edgeFlows.get(e));
				}
			}
		}
		maxFlow += localFlow;
		System.out.println(maxFlow);
		System.out.println("printed flow");
	}
	


	
	
	
	private ArrayList<Edge> getWay(Node source, Node target) {
		ArrayList<Edge> result = new ArrayList<Edge>();
		Queue<Edge> queueEdges = new LinkedList<Edge>();
		Node currentNode = source;
		Edge lastEdge = null;
		edgeDirection.clear();
		
		for (Edge e : currentNode.getNeighbors()) {
			if(e.getOne().equals(currentNode)&&e.getWeight()>edgeFlows.get(e)){
				queueEdges.add(e);
				edgeDirection.put(e, true);
			}
			}
		



		while(!queueEdges.isEmpty()) {
			Edge curEdge = queueEdges.poll();
			
			if (edgeDirection.get(curEdge)) {
				currentNode = curEdge.getTwo();
				if (currentNode.equals(target)) {
					lastEdge = curEdge;

					queueEdges.clear();
					break;
				}
			} else {
				currentNode = curEdge.getOne();
			}
			

		
			for (Edge e : currentNode.getNeighbors()){
				if(e.getOne().equals(currentNode)){
					if (!(edgeDirection.containsKey(e)) &&e.getWeight() > edgeFlows.get(e)) {

						queueEdges.add(e);
						edgeDirection.put(e, true);
						predecessor.put(e, curEdge);
					}
				}else{
					

					if (!e.getDirected()) {
						if (!(edgeDirection.containsKey(e)) && edgeFlows.get(e).equals(e.getWeight())) {
							queueEdges.add(e);
							edgeDirection.put(e, false);
							predecessor.put(e, curEdge);
						}
					} else {
						if (!(edgeDirection.containsKey(e)) && edgeFlows.get(e)>0.0) {
							queueEdges.add(e);
							edgeDirection.put(e, false);
							predecessor.put(e, curEdge);
						}
					}

				}				
			}
			

		}
		Edge currentEdge = lastEdge;
		if (currentEdge==null) {
			return result;
		}
		result.add(currentEdge);
		while (!(currentEdge.getOne()).equals(source)) {
			currentEdge = predecessor.get(currentEdge);
			result.add(currentEdge);
		}
		Collections.reverse(result);
		System.out.println(result);
		return result;
	}
	

	public LinkedList<Node> getFinalPath() {
		return finalPath;
	}


	public double getTime(){
		return time;
	}

}
