/**
 * This class is used for the edges of the graphs. Both directed and undirected.
 * 
 * @author Alexander Reichart, Andrada Boldis
 * @date October 2016
 */
package algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import components.Edge;
import components.Node;

public class BreadthFirst {

	HashMap<String, Node> nodes;
	List<Edge> edges = new ArrayList<Edge>();
	HashMap<Node, Integer> values = new HashMap<Node, Integer>();
	Queue<Node> queue = new LinkedList<Node>();

	public BreadthFirst(HashMap<String, Node> nodes, Set<Edge> edges) {
		this.nodes = nodes;
		this.edges.addAll(edges);
	}

	/**
	 * 
	 * @param source
	 *            label of start node
	 * @param target
	 *            label of end node
	 * @return String path from source to target
	 */

	public String traverse(String source, String target) {

		if (source.equals(target))
			return "Very funny.";

		if (!nodes.containsKey(source) & !nodes.containsKey(target)) {
			return "Neither node was found.";
		} else if (!nodes.containsKey(source) & nodes.containsKey(target)) {
			return "source node was not found.";
		} else if (nodes.containsKey(source) & !nodes.containsKey(target)) {
			return "target node was not found.";
		}

		Node startNode = nodes.get(source);
		this.values = new HashMap<Node, Integer>();
		this.values.put(startNode, 0);
		this.queue.add(startNode);

		while (!queue.isEmpty()) {
			Node currentNode = queue.poll();
			this.evaluateNeighbors(currentNode, this.values.get(currentNode));
		}

		String path = traverseBack(this.nodes.get(target), "");
		path = path.substring(0, path.length() - 3);

		if (path.split(">").length > 1)
			return path + " Steps taken: " + (path.split(">").length - 1);
		else {
			return "No valid path found";
		}
	}

	/**
	 * 
	 * @param source
	 *            Node to start at
	 * @param path
	 *            String path so far
	 * @return String the final path
	 */
	public String traverseBack(Node source, String path) {
		ArrayList<Edge> neighbors = source.getNeighbors();

		path = source.getLabel() + " > " + path;
		if (values.containsKey(source) && values.get(source) >= 0) {
			Node nextNode = source;
			for (Edge edge : neighbors) {
				if (values.containsKey(edge.getNeighbor(source))
						&& (!edge.getDirected() || source.equals(edge.getTwo())))
					nextNode = values.get(nextNode) < values.get(edge.getNeighbor(source)) ? nextNode
							: edge.getNeighbor(source);
			}
			if (!source.equals(nextNode))
				path = traverseBack(nextNode, path);
		} else if (!values.containsKey(source)) {
			path = "No valid Path between the nodes.";
		}
		return path;
	}

	/**
	 * 
	 * @param currentNode
	 *            node to evaluate and add to queue
	 * @param currentValue
	 *            value to assign to node
	 */
	public void evaluate(Node currentNode, int currentValue) {

		if (!this.values.containsKey(currentNode)) {
			this.values.put(currentNode, currentValue);
			queue.add(currentNode);

		}

	}

	/**
	 * 
	 * @param currentNode
	 *            node whose neighbors we are going to evaluate
	 * @param currentValue
	 *            value of current node
	 */
	public void evaluateNeighbors(Node currentNode, int currentValue) {
		ArrayList<Edge> neighbors = currentNode.getNeighbors();

		for (Edge neighbor : neighbors) {
			if (neighbor.getDirected()) {
				if (neighbor.getOne().equals(currentNode) && !neighbor.getTwo().equals(currentNode))
					evaluate(neighbor.getTwo(), currentValue + 1);
			} else {
				if (!neighbor.getOne().equals(neighbor.getTwo()))
					evaluate(neighbor.getNeighbor(currentNode), currentValue + 1);

			}

		}

	}
}
