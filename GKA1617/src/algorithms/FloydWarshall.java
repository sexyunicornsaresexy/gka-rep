/**
 * This class is used for the edges of the graphs. Both directed and undirected.
 * 
 * @author Alexander Reichart, Andrada Boldis
 * @date October 2016
 */
package algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import components.Edge;
import components.Node;

public class FloydWarshall {

	HashMap<String, Node> nodes;
	ArrayList<Node> nodesList = new ArrayList<Node>();
	List<Edge> edges = new ArrayList<Edge>();
	
	HashMap<Node, HashMap<Node, Integer>> values = new HashMap<Node, HashMap<Node, Integer>>();
	HashMap<Node, HashMap<Node, Node>> predecessor = new HashMap<Node, HashMap<Node, Node>>();
	
	int inf = 99999;
	

	LinkedList<Node> finalPath = new LinkedList<Node>(); 
	
	public FloydWarshall(HashMap<String, Node> nodes, Set<Edge> edges) {
		this.nodes = nodes;
		this.edges.addAll(edges);
		}

	/**
	 * 
	 * @param source
	 *            label of start node
	 * @param target
	 *            label of end node
	 * @return String path from source to target
	 */	
	public String traverse(String source, String target) {
		finalPath = new LinkedList<Node>(); 
		
		if (source.equals(target))
		return "Source equals target";

	if (!nodes.containsKey(source) & !nodes.containsKey(target)) {
		return "Neither node was found.";
	} else if (!nodes.containsKey(source) & nodes.containsKey(target)) {
		return "source node was not found.";
	} else if (nodes.containsKey(source) & !nodes.containsKey(target)) {
		return "target node was not found.";
	}
		
		nodesList = new ArrayList<Node>(nodes.values());
		HashMap<Node, HashMap<Node, Integer>> values = new HashMap<Node, HashMap<Node, Integer>>();
		HashMap<Node, HashMap<Node, Node>> predecessor = new HashMap<Node, HashMap<Node, Node>>();
		
		for (Node node : nodesList) {
			if(!values.containsKey(node)){
				this.values.put(node,new HashMap<Node,Integer>());			
				for (Node innerNode : nodesList) {
					this.values.get(node).put(innerNode, innerNode.equals(node) ? 0 :inf);
				}
			}
			else{
				for (Node innerNode : nodesList) {
					this.values.get(node).put(innerNode, innerNode.equals(node) ? 0 :inf);
				}				
			}
			if(!predecessor.containsKey(node)){
				this.predecessor.put(node,new HashMap<Node,Node>());			
			}
		}
		
		
		
		
		for (Edge edge : edges){
			if(edge.getDirected()){
				this.values.get(edge.getOne()).put(edge.getTwo(), edge.getWeight());
				this.predecessor.get(edge.getOne()).put(edge.getTwo(), edge.getOne());
			}else{
				this.values.get(edge.getOne()).put(edge.getTwo(), edge.getWeight());
				this.predecessor.get(edge.getOne()).put(edge.getTwo(), edge.getOne());
				
				this.values.get(edge.getTwo()).put(edge.getOne(), edge.getWeight());
				this.predecessor.get(edge.getTwo()).put(edge.getOne(), edge.getTwo());				
				
			}				
		}
		
		
		for (Node intermediateNode : nodesList) {

			for (Node fromNode : nodesList) {

              if (this.predecessor.get(fromNode).get(intermediateNode) == null) continue;
				
				for (Node toNode : nodesList) {
                    if (this.values.get(fromNode).get(toNode) 
                    		> this.values.get(fromNode).get(intermediateNode) + 
                    		(this.values.get(intermediateNode).containsKey(toNode) ? this.values.get(intermediateNode).get(toNode) : inf)) {
                    	this.values.get(fromNode).put(toNode, this.values.get(fromNode).get(intermediateNode) + this.values.get(intermediateNode).get(toNode));
                        this.predecessor.get(fromNode).put(toNode, this.predecessor.get(intermediateNode).get(toNode)); 
                    }					
				}
              if (this.values.get(fromNode).get(fromNode) < 0) {
              return "Negative circle detected";
          }				
				
			}			
			
			
			
		}

		traverseBack(this.nodes.get(source),this.nodes.get(target));
		
		if (finalPath.size()>0)
			return finalPath + "<br>Steps taken: " + finalPath.size()+ "<br>Total weight: " + this.values.get(this.nodes.get(source)).get(this.nodes.get(target));
		else {
			return "No valid path found";
		}

	}

	/**
	 * 
	 * @param source
	 *            Node to start at
	 * @param path
	 *            String path so far
	 * @return String the final path
	 */
	public void traverseBack(Node source, Node target) {
		finalPath.add(0,target);
		do{
			if(this.predecessor.get(source).containsKey(finalPath.get(0))){
				finalPath.add(0,this.predecessor.get(source).get(finalPath.get(0)));
			}else{
				finalPath = new LinkedList<Node>();
				break;
			}

		}while(!finalPath.get(0).equals(source));
		System.out.println("Final Path: " + finalPath);
	}

	public LinkedList<Node> getFinalPath() {
		return finalPath;
	}



}
